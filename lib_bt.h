#include <stdlib.h>
#include <stdio.h>

typedef struct tNo_b {
	int chave;
	struct tNo_b *dir, *esq;
} tNo_b;

// / inicializa um nó da arvore B
tNo_b* inicia_no_B (int chave);

// caso o numero tenha mais de um digito
int descobre_valor (int *i, char *input);

// recebe vetor de entrada e monta arvore B
tNo_b *percorre_entrada_B (tNo_b *no, char *input);

// monta a arvore B
tNo_b *recebe_arvore_B (tNo_b *no, int valor);

// impressao pre ordem da arvore B
void pre_ordem_B (tNo_b *node);

// impressao em ordem da arvore B
void em_ordem_B (tNo_b *no);

// funcao que exclui subarvore B
void exclui_B (tNo_b *no);

// soma a subarvore B
int soma (tNo_b *no);