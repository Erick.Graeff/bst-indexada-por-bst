# Readme - Trabalho 2 CI1057

## Autores
Carlos Eduardo Cichon Henriques | GRR20196991 <br>
Erick Graeff Petzold | GRR20193175

## Nosso trabalho
Nós começamos nosso trabalho montando a estrutura da árvore B, que era o componente mais interno do projeto. Por ser bem parecida com o trabalho 1, pudemos aproveitar boa parte dos nossos códigos nesse momento.
Depois, implementamos algumas funções que ainda não existiam, como a de impressão em pre-ordem, a exclusão e a soma. Porém, foi quando começamos a implementar a árvore A e quando tivemos uma das aulas, onde o professor disse que as duas árvores deveriam ser BST. Até então não tínhamos feito a B como BST, então tivemos que refazê-la.
Então, nós mudamos um pouco a estrutura da árvore e o esquema das funções para satisfazer essa condição de árvore binária.
Depois começamos a montar a árvore A, e as suas funções. Como já tínhamos feito tudo da B, foi bem mais fácil a integração. Implementamos primeiro o switch case que escolhe o tipo de operação e a leitura das entradas de stdin. Depois disso, fizemos a inserção funcionar na árvore A. E fizemos o método de impressão para testarmos se estava tudo certo. Na sequência implementamos o método da busca, que também foi tranquilo e testamos. O mais complicado na verdade, foi implementar a remoção. No primeiro momento achamos que estava tudo certo, mas depois de fazer vários casos de teste, percebemos que muitos deles geravam perda de memória e segfault. Então provavelmente estávamos perdendo alguns ponteiros.
Pra achar os erros, nos fomos fazendo testes de mesa, utilizando o [site de simulação](https://www.cs.usfca.edu/~galles/visualization/Algorithms.html) para vermos o comportamento da árvore e ao mesmo tempo ver o código. Acabamos achando vários pequenos problemas que consertamos e a quantidade de casos que davam erro foram diminuindo. Mesmo assim, tiveram alguns que persistiram e a última semana foi dedicada basicamente a consertá-los. Tentamos até mesmo implementar outra forma de exclusão que achamos, mas também não deu certo. Até o momento que achamos o grande erro, um ponteiro que estávamos esquecendo de atribuir, e a remoção funcionou completamente.

## Arquivos
### lib_bt.c
Arquivo que implementa a árvore B das especificações do projeto. Possui as funções:
* **inicia_no_B:** Função que cria um nó da árvore B e atribui uma chave a ele.
* **descobre_valor:** Função que descobre um valor de um parênteses. Serve para quando há mais de um dígito no número.
* **recebe_arvore_B:** Função que "monta" a árvore binária B, ou seja, percorre a árvore até achar o lugar do novo nodo, e então o adiciona, usando a inicia_no_B.
* **percorre_entrada_B:** Função que percorre a entrada de parênteses aninhados recebida. Chama a descobre_valor e então monta a árvore com o novo nó.
* **pre_ordem_B:** Imprime a árvore B em pré_ordem.
* **em_ordem_B:** Imprime a árvore B em ordem.
* **exclui_B:** Função que desaloca a memória utlizada pelos nodos da árvore B.
* **soma:** Função que retorna a soma de todos os nodos da árvore B, necessária para indexar a árvore A.

### lib_bt.h
Arquivo de cabeçalho para as funções do lib_bt.c. Além disso, define a estrutura de dados da árvore B.

---
### lib_at.c
Arquivo que implementa a árvore A das especificações do projeto. Possui as funções:
* **inicia_no_A:** Função que cria um nó da árvore A e atribui uma chave (árvore B) à ele.
* **recebe_arvore_B:** Função que monta a árvore binária A, ou seja, percorre até achar o lugar correto do novo nodo, indexado pela soma da sua chave (árvore B), e então o adiciona, usando a inicia_no_A.
* **busca:** Função que percorre a árvore A, até encontrar, ou não, o nodo especificado.
* **min:** Função que retorna o nodo mais a esquerda a partir do nodo de entrada.
* **sucessor:** Função que retorna o sucessor de um nó de entrada.
* **ajustaNoPai:** Função que ajusta o ponteiro para o nó pai de um nó após a realização de uma exclusão.
* **exclui_A:** Função que trata todos os casos de exclusão de um nó da árvore A. Ajusta todos os ponteiros e retorna a nova raiz da árvore.
* **em_ordem_A:** Função que imprime a árvore A em ordem.
* **pre_ordem_A:** Função que imprime a árvore A em formato pré ordem.

### lib_at.h
Arquivo de cabeçalho das funções do lib_at.c. Além disso, define a estrutura de dados da árvore A.

---
### busca.c
Arquivo principal do trabalho. Declara as variáveis utlizadas, e lê as entradas do programa. Possui um *switch case* que seleciona qual tipo de operação deve ser executada, i para inserção, b para busca e r para remoção. Depois de verificar qual operação deve ser feita, ele chama o método adequado das libs.

### busca
Executável do trabalho. Basta digitar ./busca < [nomeArquivo.txt] para rodar.

### Makefile 
Para compilar o projeto basta rodar `make`. Rodando `make clean` os arquivos temporários serão removidos. Rodando `make purge` tudo que não for o código principal será removido.

---
## Considerações
* A especificação na página do trabalho, cita a funcionalidade 4 da árvore A como impressão em ordem, entretando, no exemplo de saída, a impressão é pre-ordem. Por via das dúvidas, fizemos as duas formas, e deixamos comentada a pré-ordem nas linhas 37 e 60 da busca.c;
* A especificação na página do trabalho, cita a funcionalidade 2 da árvore B como impressão em ordem, entretando, no exemplo de saída, a impressão é pre-ordem. Por via das dúvidas, fizemos as duas formas, e deixamos comentada a em-ordem nas linhas 43, 182 e 193 de lib_at.c;