#include "lib_at.h"
#include <stdio.h>

//temp
#define TAM 1000

int main () {
  int somaEntrada;
  tNo_a *no_a;
  tNo_b *no_b = NULL;

  char entrada [TAM];
  // le a flag, espaço e vetor
  // da primeira insercao
  char c = fgetc (stdin);
  fgetc (stdin);
  fgets (entrada, TAM, stdin);
  if (c == 'i'){
    no_a = recebe_arvore_A (NULL, percorre_entrada_B (no_b, entrada));
    em_ordem_A(no_a);
  }

  
  c = fgetc (stdin);
  while (c != EOF) {
    // le espaco e subarvore B
    fgetc (stdin);
    fgets (entrada, TAM, stdin);

    switch (c) {
      case 'i':
        // monta a arvore A passando
        // a arvore B como parametro
        printf("Incluindo: %s\n", entrada);
        recebe_arvore_A (no_a, percorre_entrada_B (no_b, entrada));
        em_ordem_A(no_a);
        // pre_ordem_A(no_a);
        printf("\n");
        break;
      case 'b':
        // monta subarvore B e 
        // calcula a soma da mesma
        printf("Buscando: %s\n", entrada);
        tNo_b *busca_b = percorre_entrada_B (NULL, entrada);
        somaEntrada = soma (busca_b);
        if (busca (no_a, somaEntrada, 1))
          printf ("Valor encontrado\n");
        else
          printf ("Esse valor não foi encontrado\n");
        exclui_B(busca_b);
        printf("\n");
        break;
      case 'r':
        // monta subarvore B e 
        // calcula a soma da mesma
        printf("Excluindo: %s\n", entrada);
        tNo_b *remove_b = percorre_entrada_B (NULL, entrada);
        somaEntrada = soma (remove_b);
        exclui_B(remove_b);
        no_a = exclui_A (no_a, somaEntrada);
        em_ordem_A (no_a);
        // pre_ordem_A (no_a);
        printf("\n");
        break;
      default:
        break;
    }
    // le proxima flag
    c = fgetc (stdin);
  }

  printf ("\n");
  return 0;
}