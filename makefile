CC = gcc
CFLAGS = -Wall

OBJ = lib_bt.o lib_at.o busca.o
LIB = lib_bt.h lib_at.h
EXECUTAVEL = busca

all: $(EXECUTAVEL)

# arquivo executavel
busca: $(OBJ)

# arquivos objetos
lib_bt.o : $(LIB) lib_bt.c
lib_at.o : $(LIB) lib_at.c
busca.o  : $(LIB) busca.c lib_bt.c lib_at.c

# remove arquivos temporarios
clean:
	-rm -f *~ *.o

#remove tudo o que nao for o codigo fonte original
purge: clean
	-rm -f $(EXECUTAVEL)