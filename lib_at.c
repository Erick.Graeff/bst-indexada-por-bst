#include "lib_at.h"

// recebe subarvore B
// e indexa na arvore A
tNo_a* inicia_no_A (tNo_b *chave) {
	tNo_a *n = (tNo_a *)malloc (sizeof (tNo_a));
	n->chave = chave;
	n->esq = NULL;
	n->dir = NULL;
	n->pai = NULL;
	return n;
}

// procura lugar onde subarvore B
// deve ser indexada
tNo_a *recebe_arvore_A (tNo_a *no, tNo_b *input) {
	if (no == NULL)
		return inicia_no_A (input);

	if (soma (input) < soma (no->chave)) {
		no->esq = recebe_arvore_A (no->esq, input);
		no->esq->pai = no;
	}
	else {
		no->dir = recebe_arvore_A (no->dir, input);
		no->dir->pai = no;
	}
	return no;
}

// busca um determinado nodo A passando
// a raiz, valor de indexacao do nodo e
// flag de impressao
tNo_a* busca (tNo_a *no_a, int somaEntrada, int imprime) {
	if (no_a == NULL)
		return NULL;
	
	int somaNo = soma (no_a->chave);

	// flag
	if (imprime) {
		pre_ordem_B (no_a->chave);
		// em_ordem_B (no_a->chave);
		printf (": %d\n", somaNo);
	}
	

	if (somaNo == somaEntrada)
		return no_a;

	if (somaNo > somaEntrada)
		return busca (no_a->esq, somaEntrada, imprime);

	return busca (no_a->dir, somaEntrada, imprime);
}

// retorna o valor minimo
tNo_a *min (tNo_a *no) {
	if (no->esq == NULL)
		return no;
	else
		return min (no->esq);
}

// retorna o sucessor de um determinado elemento
tNo_a *sucessor (tNo_a *no) {
	tNo_a *s = NULL;
	if (no->dir != NULL)
		return min (no->dir);
	else {
		s = no->pai;
		while (s != NULL && no == s->dir) {
			no = s;
			s = s->pai;
		}   
	}
	return s;
}

// ajusta o no pai ao fazer uma exclusao
void ajustaNoPai (tNo_a *no, tNo_a *novo) {
	if (no->pai != NULL) {
		if (no->pai->esq == no)
			no->pai->esq = novo;
		else
			no->pai->dir = novo;

		if (novo != NULL)
			novo->pai = no->pai;
	}
}

 /* tNo_a *exclui_A_versao2 (tNo_a *raiz_a, int somaEntrada){
	tNo_a *temp;
	int soma_a = soma(raiz_a->chave);
	if (raiz_a == NULL){
		return NULL;
	}
	else if(somaEntrada < soma_a){
		raiz_a->esq = exclui_A_indiano(raiz_a->esq, somaEntrada);
	}
	else if(somaEntrada > soma_a){
		raiz_a->dir = exclui_A_indiano(raiz_a->dir, somaEntrada);
	}
	else{
		if (raiz_a->esq == NULL){
			temp = raiz_a->dir;
			exclui_B(raiz_a->chave);
			free(raiz_a);
			return temp;
		}
		else if(raiz_a->dir == NULL){
			temp = raiz_a->esq;
			exclui_B(raiz_a->chave);
			free(raiz_a);
			return temp;
		}	
		else{
			temp = min(raiz_a->dir);
			printf ("sucessor do %d eh o %d\n", soma (raiz_a->chave), soma (temp->chave));
			// exclui_B (raiz_a->chave);
			raiz_a->chave = temp->chave;
			raiz_a->dir = exclui_A_indiano(raiz_a->dir, soma(temp->chave));
		}	
	}
	return raiz_a;
} */


// exclui um determinado nodo A passando
// a raiz e o valor de indexacao do nodo
tNo_a *exclui_A (tNo_a *raiz_a, int somaEntrada) {
	tNo_a *del = busca (raiz_a, somaEntrada, 0);
	tNo_a *nova_raiz = raiz_a;

	if (del->esq == NULL) {
		if (raiz_a == del) {
			nova_raiz = del->dir;
			if (del->dir != NULL)
				del->dir->pai = NULL;
		}

		ajustaNoPai (del, del->dir);
	}
	else if (del->dir == NULL) {
		if(raiz_a == del) {
			nova_raiz = del->esq;
			if (del->esq != NULL)
				del->esq->pai = NULL;
		}

		ajustaNoPai (del, del->esq);
	}
	else {
		tNo_a *suc = min (del->dir);

		ajustaNoPai (suc, suc->dir);
		suc->esq = del->esq;
		suc->dir = del->dir;
		ajustaNoPai (del, suc);
		del->esq->pai = suc;

		if (del == raiz_a)
			nova_raiz = suc;
	}
	exclui_B (del->chave);
	free (del);
	return nova_raiz;
}

// imprime arvore A em ordem
void em_ordem_A (tNo_a *no) {
	if (no != NULL) {
		printf ("[");
		em_ordem_A (no->esq);
		pre_ordem_B (no->chave);
		// em_ordem_B (no->chave);
		printf (": %d\n", soma (no->chave));
		em_ordem_A (no->dir);
		printf ("]\n");
	}
}

void pre_ordem_A (tNo_a *no){
	if (no != NULL){
		printf("[");
		pre_ordem_B(no->chave);
		// em_ordem_B(no->chave);
		printf (": %d\n", soma (no->chave));
		pre_ordem_A(no->esq);
		pre_ordem_A(no->dir);
		printf("]\n");
	}
	
}