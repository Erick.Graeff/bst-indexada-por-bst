#include "lib_bt.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// inicializa um nó da arvore B
tNo_b* inicia_no_B (int chave) {
	tNo_b *n = (tNo_b *)malloc (sizeof (tNo_b));
	n->chave = chave;
	n->esq = NULL;
	n->dir = NULL;
	return n;
}

// caso o numero tenha mais de um digito
int descobre_valor (int *i, char *input) {
	char valor [15];
	int j = 0;
	while (input[*i] >= 48 && input[*i] <= 57) { 
		valor[j] = input[*i];
		j++;
		(*i)++;
	}
	valor[j] = '\0';
	return atoi (valor);
}

// monta a arvore B
tNo_b *recebe_arvore_B (tNo_b *no, int valor) {
	if (no == NULL)
		return inicia_no_B (valor);

	if (valor < no->chave)
		no->esq = recebe_arvore_B (no->esq, valor); 
	else
		no->dir = recebe_arvore_B (no->dir, valor);
	
	return no;
}

// recebe vetor de entrada e monta arvore B
tNo_b *percorre_entrada_B (tNo_b *no, char *input) {
	int valor=0;
	int i=0;
	while (i < strlen (input)) {
		if (input[i] == '(' ) {
			i++;
			valor = descobre_valor (&i,input);
			no = recebe_arvore_B (no, valor);
		}
		else {
			i++;
		}
	}
	return no;
}

// impressao pre ordem da arvore B
void pre_ordem_B (tNo_b *no) {
	if (no != NULL) {
		printf ("(");
		if (no->chave != 0) {
			printf ("%d", no->chave);
			pre_ordem_B (no->esq);
			pre_ordem_B (no->dir);
			printf (")");
		}
		else {
			printf (")");
			pre_ordem_B (no->esq);
			pre_ordem_B (no->dir);
		}
	}
}

void em_ordem_B (tNo_b *no) {
	if (no != NULL) {
		printf ("(");
		if (no->chave != 0) {
			pre_ordem_B (no->esq);
			printf ("%d", no->chave);
			pre_ordem_B (no->dir);
			printf (")");
		}
		else {
			pre_ordem_B (no->esq);
			printf (")");
			pre_ordem_B (no->dir);
		}
	}
}

// funcao que exclui subarvore B
void exclui_B (tNo_b *no) {
    if (no != NULL) {
        exclui_B (no->esq);
        exclui_B (no->dir);
        free (no);
    }
}

// soma a subarvore B
int soma (tNo_b *no) {
	if (no == NULL)
		return 0;
			
	return no->chave + soma (no->esq) + soma (no->dir);
}