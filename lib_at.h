#include "lib_bt.h"

typedef struct tNo_a {
  tNo_b *chave;
  struct tNo_a *dir, *esq, *pai;
} tNo_a;

// recebe subarvore B
// e indexa na arvore A
tNo_a* inicia_no_A (tNo_b *chave); 

// procura lugar onde subarvore B
// deve ser indexada
tNo_a *recebe_arvore_A (tNo_a *no, tNo_b *input);

// busca um determinado nodo A passando
// a raiz, valor de indexacao do nodo e
// flag de impressao
tNo_a* busca (tNo_a *no_a, int somaEntrada, int imprime);

// retorna o valor minimo
tNo_a *min (tNo_a *no);

// retorna o sucessor de um determinado elemento
tNo_a *sucessor (tNo_a *no);

// ajusta o no pai ao fazer uma exclusao
void ajustaNoPai (tNo_a *no, tNo_a *novo);

// exclui um determinado nodo A passando
// a raiz e o valor de indexacao d
tNo_a *exclui_A (tNo_a *no_a, int somaEntrada);

// tNo_a *exclui_A_versao2 (tNo_a *raiz_a, int somaEntrada);

// imprime arvore A em ordem
void em_ordem_A (tNo_a *no);

void pre_ordem_A (tNo_a *no);